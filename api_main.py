#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import os
import numpy as np
from numpy import arange
import re
from datetime import datetime
import time
from json import dumps, load
from datetime import timedelta, date
from collections import OrderedDict
import datetime
import psycopg2 as ps
from psycopg2.extensions import AsIs
import re
import falcon
import json
import sys

PTILE_THRESHOLD = 20


def get_product_advice(pid, price):
    
    eps = 0.000001
    
    advice = -1
    confidence = 0
    
    conn_laptops=ps.connect("dbname='bitflip_pilot_database' user='postgres' host='localhost' password='hkhk'")
    cur_laptops = conn_laptops.cursor()
    
    cur_laptops.execute("SELECT price_data from laptops where pid=%(pid)s", {'pid':pid})
    result = cur_laptops.fetchall()
    
    print str(result)
    
    if len(result):
        price_data_db = json.loads(result[0][0])
    else:
        return advice, confidence
    
    cur_laptops.close()
    conn_laptops.close()
    
    ptile_price = np.percentile(price_data_db, PTILE_THRESHOLD)
    print str(ptile_price)
    if price <= ptile_price:
        min_price = np.min(price_data_db)
        advice = 1
        if price <= min_price:
            confidence = 100
        else:
            confidence = 50 * np.float(ptile_price - price)/(ptile_price - min_price + eps) + 50
    else:
        max_price = np.max(price_data_db)
        advice = 0
        if price >= max_price:
            confidence = 100
        else:
            confidence = 50 * np.float(price - ptile_price)/(max_price - ptile_price + eps) + 50
        
        
    return advice, int(confidence), price_data_db
    
    
class QueryHandler:
    
    def on_post(self, req, resp):
        
        body = req.stream.read()
        print str(body)
        
        body = json.loads(str(body)) #body has the json data
        #print str(body)
        
        pid = str(body['pid'])
        price_string = body['price']
        
        if ('R' in price_string) or (',' in price_string):
            price = int(''.join(re.findall(r'\d+', price_string))) 
        else:
            price = int(price_string)
        
        advice, confidence, price_data = get_product_advice(pid, price)
        
        #advice = 1
        #confidence = 100
        
        resp.body = '{"advice": "%s", "confidence": "%s", "price_data": "%s"}' % (str(advice), str(confidence), str(price_data))
        resp.status = falcon.HTTP_201
        resp.set_headers(body)
        
app = falcon.API()

app.add_route('/ping', QueryHandler())