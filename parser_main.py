#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta, date
from collections import OrderedDict
from bs4 import BeautifulSoup
import datetime
import psycopg2 as ps
from psycopg2.extensions import AsIs
import re

#currently searching for appliances and electronics only
NUMBER_OF_ATTEMPTS = 10

DATA_LIMIT = 60

def query_to_results(query):
    print query

    data_dict = {}

    for attempts in range(0, NUMBER_OF_ATTEMPTS):
        try:
            page = requests.get(query)
            soup = BeautifulSoup(page.text, "lxml")
            number_of_products_per_page = len(soup.find('div', id ="products").find_all('div', class_='gd-col gu3'))
            if number_of_products_per_page == 0:
                number_of_products_per_page = len(soup.find('div', id ="products").find_all('div', class_='gd-col gu4'))
            total_number_of_products = int(''.join(re.findall(r'\d+', soup.find('span', class_='items').string)))
            if total_number_of_products > 1500:
                total_number_of_products = 1500

            for start in range(1, total_number_of_products, number_of_products_per_page):      
                search_query = query + '&start=' + str(start)
                page = requests.get(search_query)
                #get the json contents
                soup = BeautifulSoup(page.text, "lxml")
                products_panel = soup.find('div', id ="products")
                list_of_products = products_panel.find_all('div', class_='gd-col gu3')
                if not (list_of_products):
                    list_of_products = products_panel.find_all('div', class_='gd-col gu4')

                for i in range(len(list_of_products)):
                    curr_product_data = list_of_products[i]
                    if (curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design ")):
                        pid = curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design ").get('data-pid')
                    elif(curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design  quickview-required")):
                        pid = curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design  quickview-required").get('data-pid')
                    elif(curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design  quickview-required vs-widget-required")):
                        pid = curr_product_data.find('div', class_="product-unit unit-4 browse-product new-design  quickview-required vs-widget-required").get('data-pid')
                    elif(curr_product_data.find('div', class_="product-unit unit-3 browse-product new-design quickview-required")):
                        pid = curr_product_data.find('div', class_="product-unit unit-3 browse-product new-design quickview-required").get('data-pid')
                    elif(curr_product_data.find('div', class_="product-unit unit-3 browse-product new-design quickview-required vs-widget-required")):
                        pid = curr_product_data.find('div', class_="product-unit unit-3 browse-product new-design quickview-required vs-widget-required").get('data-pid')
                    else:
                        pid = curr_product_data.find('div', class_="product-unit unit-3 browse-product new-design ").get('data-pid')
                    price_string = curr_product_data.find('div', class_="pu-final").span.string
                    try:
                        price_int = int(''.join(re.findall(r'\d+', price_string)))           
                    except:
                        print price_string + " could not be regeXed !!"
                        price_int = 0

                    #print pid + " -- " + price_string + ' ---- ' + str(price_int)
                    data_dict[pid] = price_int

        except:
            print "failed attempt : " + str(attempts)
            continue
        break

    return data_dict


query_list = ['http://www.flipkart.com/laptops/pr?sid=6bo,b5g' ,
         'http://www.flipkart.com/mobiles/pr?p%5B%5D=facets.type%255B%255D%3DSmartphones&sid=tyy%2C4io&filterNone=true',
         'http://www.flipkart.com/home-entertainment/televisions/pr?sid=ckf,czl&otracker=clp_televisions_CategoryLinksModule_0-2_catergorylinks_0_VIEWALL',
         'http://www.flipkart.com/cameras/pr?sid=jek%2Cp31&filterNone=true&q=Camera',
         'http://www.flipkart.com/home-kitchen/home-appliances/refrigerators/pr?sid=j9e%2Cabm%2Chzg&otracker=clp_home-kitchen-home-appliances-refrigerators_clp%2FBreadCrumbModule_0-0_Refrigerators',
         'http://www.flipkart.com/home-kitchen/home-appliances/washing-machines/pr?sid=j9e%2Cabm%2C8qx&otracker=ch_vn_washingmac_filter_Brands_SEE%20ALL%20WASHING%20MACHINES',
         'http://www.flipkart.com/home-kitchen/home-appliances/air-conditioners/pr?sid=j9e,abm,c54&otracker=ch_vn_airconditi_filter_Brands_SEE%20ALL',
         'http://www.flipkart.com/computers/audio-players/home-audio/speakers/pr?sid=6bo,ord,rlj,8sb']

for query in query_list:
    data_dict = query_to_results(query)
    
    #print data_dict

    conn_laptops=ps.connect("dbname='bitflip_pilot_database' user='postgres' host='localhost' password='hkhk'")
    cur_laptops = conn_laptops.cursor()

    for pid in data_dict.keys():
        price_int = data_dict[pid]
        cur_laptops.execute("SELECT price_data from laptops where pid=%(pid)s", {'pid': pid})
        result = cur_laptops.fetchall()
        if len(result):
            price_data_db = json.loads(result[0][0])
        else:
            price_data_db = []

        #update the price data
        price_data_db = [price_int] + price_data_db
        
        #limit data to some number of entries
        price_data_db = price_data_db[:DATA_LIMIT]

        if len(price_data_db) == 1:
            cur_laptops.execute("""INSERT INTO laptops VALUES (%(pid)s, %(price_data)s)""", 
                    {'pid': pid, 'price_data': json.dumps(price_data_db)})

        else:
            cur_laptops.execute("""UPDATE laptops SET price_data=%(price_data)s WHERE pid = %(pid)s""", 
                            {'pid': pid, 'price_data': json.dumps(price_data_db)})


    conn_laptops.commit()
    cur_laptops.close()
    conn_laptops.close()